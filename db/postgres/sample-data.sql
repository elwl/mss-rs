INSERT INTO users (
    username, pass_hash,
    pin, gender, accepted_eula
) VALUES (
    'admin', 'admin',
    '1234', 'Male', 'true'
);
INSERT INTO users (
    username, pass_hash,
    pin, gender, block_reason, unblock_date
) VALUES (
    'banned', '$2a$12$aktwRx5l.6h0aoeqzAcBfesdRdwgWmkTdswVca1OMkCcs3TELo/1e',
    '1234', 'Male', 2, '2016-01-01'::timestamp
);
INSERT INTO characters (
    name, account_id, face, hair,
    hp, max_hp, mp, max_mp,
    str, dex, int, luk
) VALUES (
    'chara한글', 1, 20000, 30000,
    50, 50, 5, 5,
    13, 4, 4, 4
);
INSERT INTO characters (
    name, account_id, face, hair,
    hp, max_hp, mp, max_mp,
    str, dex, int, luk
) VALUES (
    'aaaaa1', 1, 20001, 30007,
    50, 50, 5, 5,
    13, 4, 4, 4
);
