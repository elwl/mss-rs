#!/bin/sh

set -e

psql $1 -f users.sql
psql $1 -f characters.sql

# sample data
if [ $# -gt 1 ]; then
    psql $1 -f sample-data.sql
fi
