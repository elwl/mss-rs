DROP TABLE IF EXISTS characters CASCADE;

CREATE TABLE characters (
    id          SERIAL PRIMARY KEY,
    account_id  INTEGER NOT NULL,
    name        TEXT UNIQUE NOT NULL,
    gender      SMALLINT NOT NULL DEFAULT 0,
    skin        SMALLINT NOT NULL DEFAULT 0,
    face        INTEGER NOT NULL DEFAULT 0,
    hair        INTEGER NOT NULL DEFAULT 0,
    pet_serials BIGINT ARRAY[3] NOT NULL DEFAULT '{0, 0, 0}',
    level       SMALLINT NOT NULL DEFAULT 0,
    job         SMALLINT NOT NULL DEFAULT 0,
    str         SMALLINT NOT NULL DEFAULT 0,
    dex         SMALLINT NOT NULL DEFAULT 0,
    int         SMALLINT NOT NULL DEFAULT 0,
    luk         SMALLINT NOT NULL DEFAULT 0,
    hp          SMALLINT NOT NULL DEFAULT 0,
    max_hp      SMALLINT NOT NULL DEFAULT 0,
    mp          SMALLINT NOT NULL DEFAULT 0,
    max_mp      SMALLINT NOT NULL DEFAULT 0,
    ap          SMALLINT NOT NULL DEFAULT 0,
    sp          SMALLINT NOT NULL DEFAULT 0,
    exp         INTEGER NOT NULL DEFAULT 0,
    fame        SMALLINT NOT NULL DEFAULT 0,
    map         INTEGER NOT NULL DEFAULT 0,
    portal      SMALLINT NOT NULL DEFAULT 0
);
