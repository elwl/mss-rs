use prelude::*;
use crypto::Crypto;
use packet;

use std::io::Cursor;
use std::net;

use Error;
use super::Stream;

pub enum OperationResult {
    Success(usize),
    WouldBlock,
}

pub struct Client {
    pub addr:     net::SocketAddr,
    pub crypto:   Crypto,
    stream:       Stream,

    write_buffer: Vec<u8>,
    read_buffer:  Vec<u8>,
}

impl Client {
    pub fn new (addr: net::SocketAddr, stream: Stream) -> Client {
        Client {
            addr: addr,
            stream: stream,
            crypto: Crypto::new(None, None),
            write_buffer: Vec::new(),
            read_buffer: Vec::new(),
        }
    }

    pub fn as_ref (&self) -> &Stream {
        &self.stream
    }
}

// read functions
impl Client {
    fn pending_read_length (&self) -> Option<u16> {
        // to grab the length of the packet we're waiting for, take the first two 16-bit integers
        // and xor them together
        if self.read_buffer.len() >= 4 {
            let first = (self.read_buffer[0] as u16) | ((self.read_buffer[1] as u16) << 8);
            let second = (self.read_buffer[2] as u16) | ((self.read_buffer[3] as u16) << 8);
            Some(first ^ second)
        } else {
            None
        }
    }

    pub fn flush_read (&mut self) -> Result<OperationResult, Error> {
        let mut buf = vec![0u8; 1024];

        match try!(self.stream.try_read(&mut buf)) {
            None => Ok(OperationResult::WouldBlock),
            Some(s) => {
                // tack this onto the buffer
                self.read_buffer.extend(buf.into_iter().take(s));
                Ok(OperationResult::Success(s))
            },
        }
    }

    pub fn try_read (&mut self) -> Result<Option<packet::client::Packet>, Error> {
        // do we have enough for a packet?
        let length = match self.pending_read_length() {
            None => return Ok(None),
            Some(s) => s + 4,   // the additional 4 bytes is for the length
        } as usize;

        if self.read_buffer.len() < length {
            return Ok(None);
        }

        { // TODO there's probably a better place to put these checks
            use std::mem;
            use crypto::{crc32, Iv};
            let &Iv(v) = &self.crypto.decrypt_iv;
            let recv_seq: u32 = unsafe { mem::transmute::<[u8; 4], u32>(v) };
            let raw_seq: u16 = (self.read_buffer[0] as u16) | ((self.read_buffer[1] as u16) << 8);

            assert!(raw_seq ^ (recv_seq >> 16) as u16 == 59, "incorrect packet header sequencing");

            if recv_seq as u16 % 31 == 0 {
                println!("this decrypted MUST be a Security MemoryHash packet, containing the \
                CRC {0:08x} ({1:?})", crc32::calc_crc32(0, &v[0..2]), crc32::calc_crc32(0, &v[0..2]));
            }
        }

        // we know we have enough, so pull out the packet's bytes
        let mut buf = self.read_buffer[4..length].to_vec(); // we don't care about the first 4 bytes
        self.read_buffer = self.read_buffer[length..].to_vec();

        // decrypt the buffer
        self.crypto.decrypt(&mut buf);

        let mut cursor = Cursor::new(&buf[..]);

        // now try to read a packet
        Ok(Some(
                try!(packet::client::Packet::read(&mut cursor))
               ))
    }

    pub fn try_read_all (&mut self) -> Result<Vec<packet::client::Packet>, Error> {
        let mut packets = Vec::new();
        while let Some(packet) = try!(self.try_read()) {
            packets.push(packet);
        }

        Ok(packets)
    }
}

// write functions
impl Client {
    pub fn queue_connect (&mut self, packet: packet::server::Connect) -> Result<(), Error> {
        //write content to intermediate buffer because length is needed for header
        let mut content = Vec::new();
        try!(binary::write(&mut content, &packet));

        //write header
        let mut header = Vec::new();
        try!(binary::write(&mut header, &(content.len() as u16)));
        self.write_buffer.extend(header.into_iter());

        //content is not encrypted in handshake packet
        //self.crypto.encrypt(&mut content);
        
        //write the content
        self.write_buffer.extend(content.into_iter());
        Ok(())
    }

    pub fn queue_write (&mut self, packet: packet::server::Packet) -> Result<(), Error> {
        //write content to intermediate buffer because length is needed for header
        let mut content = Vec::new();
        try!(binary::write(&mut content, &packet));

        //write header
        let mut header = Vec::new();
        try!(packet.write_header(&mut header, &self.crypto, content.len()));
        self.write_buffer.extend(header.into_iter());

        //encrypt the content
        self.crypto.encrypt(&mut content);
        
        //write the encrypted content
        self.write_buffer.extend(content.into_iter());
        Ok(())
    }

    pub fn flush_write (&mut self) -> Result<OperationResult, Error> {
        match try!(self.stream.try_write(&self.write_buffer[..])) {
            None => Ok(OperationResult::WouldBlock),
            Some(s) => {
                self.write_buffer = self.write_buffer[s..].to_vec();
                Ok(OperationResult::Success(s))
            },
        }
    }
}
