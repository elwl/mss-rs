#[macro_use] extern crate log;
extern crate mio;

extern crate ms_config as config;
extern crate ms_core as core;
extern crate ms_crypto as crypto;
extern crate ms_packet as packet;

pub mod error;
pub mod handler;
pub mod loop_;
mod prelude {
    pub use core::prelude::*;
    pub use mio::*;
}

pub use std::sync::mpsc::{Sender, Receiver};
pub use error::Error;
pub use handler::{Handler, InputMessage, OutputMessage};
pub use loop_::{Loop, Control};
pub use mio::Token;

use std::sync::mpsc;
pub fn channel () -> (Sender<handler::OutputMessage>, Receiver<handler::OutputMessage>) {
    mpsc::channel()
}
