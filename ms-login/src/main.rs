#![feature(scoped)]

extern crate chrono;
extern crate env_logger;
#[macro_use] extern crate log;
extern crate postgres;

extern crate ms_core as mscore;
extern crate ms_loop as msloop;
extern crate ms_packet as mspacket;

mod prelude {
    pub use mspacket;
    pub use msloop;
    pub use msloop::Token;
    pub use mscore;
    pub use mscore::prelude::*;

    pub use std::error::Error;
}
mod server;

fn main() {
    env_logger::init().unwrap();

    let server = server::Server::new("0.0.0.0:8484").unwrap();
    server.run().unwrap();
}
