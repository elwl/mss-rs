use prelude::*;

use mspacket::character_stat::CharacterStat;

mod provider;
pub use self::provider::Provider;

#[derive(Clone, Debug)]
pub struct Character {
    pub account_id: mscore::AccountId,
    pub character_stat: CharacterStat
}
