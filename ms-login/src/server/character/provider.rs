use prelude::*;

use super::Character;
use mspacket::character_stat::CharacterStat;

pub trait Provider {
    fn get_by_account_id(&mut self, account_id: mscore::AccountId) -> Result<Vec<Character>, Box<Error>>;
    fn get_by_character_id(&mut self, character_id: u32) -> Result<Option<Character>, Box<Error>>;
    fn save        (&mut self, user: &Character) -> Result<u64, Box<Error>>;
}

impl<T: postgres::GenericConnection> Provider for T {
    fn get_by_account_id(&mut self, account_id: mscore::AccountId) -> Result<Vec<Character>, Box<Error>> {
        let statement = try!(self.prepare(
        "SELECT id, name, gender, skin,
            face, hair, pet_serials, level,
            job, str, dex, int, luk, hp,
            max_hp, mp, max_mp, ap, sp,
            exp, fame, map, portal
         FROM characters 
         WHERE account_id=$1"
         ));
        let result = try!(statement.query(&[&(account_id.0 as i32)]));
        Ok(result.iter().map(|row| 
            Character {
                account_id: account_id,
                character_stat: CharacterStat {
                    character_id: row.get::<_, i32>(0) as u32,
                    character_name: row.get(1),
                    gender: row.get::<_, i16>(2) as u8,
                    skin: row.get::<_, i16>(3) as u8,
                    face: row.get::<_, i32>(4) as u32,
                    hair: row.get::<_, i32>(5) as u32,
                    pet_serials: {
                        [0u64; 3]
                    },
                    level: row.get::<_, i16>(7) as u8,
                    job: row.get::<_, i16>(8) as u16,
                    str: row.get::<_, i16>(9) as u16,
                    dex: row.get::<_, i16>(10) as u16,
                    int: row.get::<_, i16>(11) as u16,
                    luk: row.get::<_, i16>(12) as u16,
                    hp: row.get::<_, i16>(13) as u16,
                    max_hp: row.get::<_, i16>(14) as u16,
                    mp: row.get::<_, i16>(15) as u16,
                    max_mp: row.get::<_, i16>(16) as u16,
                    ap: row.get::<_, i16>(17) as u16,
                    sp: row.get::<_, i16>(18) as u16,
                    exp: row.get::<_, i32>(19) as u32,
                    fame: row.get::<_, i16>(20) as u16,
                    map: row.get::<_, i32>(21) as u32,
                    portal: row.get::<_, i16>(22) as u8,
                }
            }).collect::<Vec<_>>())
        
    }
    fn get_by_character_id(&mut self, character_id: u32) -> Result<Option<Character>, Box<Error>> {
        let statement = try!(self.prepare(
        "SELECT account_id, name, gender, skin,
            face, hair, pet_serials, level,
            job, str, dex, int, luk, hp,
            max_hp, mp, max_mp, ap, sp,
            exp, fame, map, portal
         FROM characters 
         WHERE id=$1"
         ));
        let result = try!(statement.query(&[&(character_id as i32)]));
        let rows = result.iter().collect::<Vec<_>>();
        Ok(if rows.len() < 1 {
            None
        } else {
            Some(Character {
                account_id: mscore::AccountId(rows[0].get::<_, i32>(0) as u32),
                character_stat: CharacterStat {
                    character_id: character_id,
                    character_name: rows[0].get(1),
                    gender: rows[0].get::<_, i16>(2) as u8,
                    skin: rows[0].get::<_, i16>(3) as u8,
                    face: rows[0].get::<_, i32>(4) as u32,
                    hair: rows[0].get::<_, i32>(5) as u32,
                    pet_serials: { [0u64; 3] },
                    level: rows[0].get::<_, i16>(7) as u8,
                    job: rows[0].get::<_, i16>(8) as u16,
                    str: rows[0].get::<_, i16>(9) as u16,
                    dex: rows[0].get::<_, i16>(10) as u16,
                    int: rows[0].get::<_, i16>(11) as u16,
                    luk: rows[0].get::<_, i16>(12) as u16,
                    hp: rows[0].get::<_, i16>(13) as u16,
                    max_hp: rows[0].get::<_, i16>(14) as u16,
                    mp: rows[0].get::<_, i16>(15) as u16,
                    max_mp: rows[0].get::<_, i16>(16) as u16,
                    ap: rows[0].get::<_, i16>(17) as u16,
                    sp: rows[0].get::<_, i16>(18) as u16,
                    exp: rows[0].get::<_, i32>(19) as u32,
                    fame: rows[0].get::<_, i16>(20) as u16,
                    map: rows[0].get::<_, i32>(21) as u32,
                    portal: rows[0].get::<_, i16>(22) as u8,
                }
            })
        })
    }
    fn save(&mut self, character: &Character) -> Result<u64, Box<Error>> {
        let statement = try!(self.prepare(
            "UPDATE characters
             SET    account_id=$2, name=$3, gender=$4, skin=$5, face=$6, hair=$7, pet_serials=$8,
                    level=$9, job=$10, str=$11, dex=$12, int=$13, luk=$14, hp=$15,
                    max_hp=$16, mp=$17, max_mp=$18, ap=$19, sp=$20, exp=$21, fame=$22,
                    map=$23, portal=$24
             WHERE  id=$1"
             ));
        Ok(try!(statement.execute(&[
                                  &(character.character_stat.character_id as i32), &(character.account_id.0 as i32), 
                                  &character.character_stat.character_name, &(character.character_stat.gender as i16),
                                  &(character.character_stat.skin as i16), &(character.character_stat.face as i32),
                                  &(character.character_stat.hair as i32), &postgres::types::Slice(&[0i64; 3]),
                                  &(character.character_stat.level as i16), &(character.character_stat.job as i16),
                                  &(character.character_stat.str as i16), &(character.character_stat.dex as i16), 
                                  &(character.character_stat.int as i16), &(character.character_stat.luk as i16), 
                                  &(character.character_stat.hp as i16), &(character.character_stat.max_hp as i16),
                                  &(character.character_stat.mp as i16), &(character.character_stat.max_mp as i16),
                                  &(character.character_stat.ap as i16), &(character.character_stat.sp as i16),
                                  &(character.character_stat.exp as i32), &(character.character_stat.fame as i16),
                                  &(character.character_stat.map as i32), &(character.character_stat.portal as i16)
                                  ])))
    }
    /*fn save (&mut self, user: &User) -> Result<u64, Box<Error>> {
        let statement = try!(self.prepare(
            "UPDATE users
             SET    username=$2, pass_hash=$3, pin=$4, gender=$5, block_reason=$6, 
                    unblock_date=$7, is_gm=$8, is_online=$9, login_time=$10,
                    accepted_eula=$11
             WHERE  id=$1"
             ));
        Ok(try!(statement.execute(&[&(user.id.0 as i64), &user.username, &user.pass_hash, &user.pin,
                                          &user.gender, &user.block_reason, &user.unblock_date,
                                          &user.is_gm, &user.is_online, &user.login_time,
                                          &user.accepted_eula])))
    }*/
}
