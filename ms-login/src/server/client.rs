use std::net;

use super::user::User;
use super::character::Character;

#[derive(PartialEq, Debug)]
pub enum LoginState {
    Disconnect, // TODO using this as a placeholder until i figure out a better idea for how to handle situations where the client should disconnect, as seen in proc_confirm_eula
    CheckPassword,
    CheckPin,
    CreatePin,
    UpdatePin,
    ConfirmGender,
    ConfirmEULA,
    WCSelect,
    CharacterSelect,
    LogoutWorld,
}

#[derive(Debug)]
pub struct Client {
    pub addr: net::SocketAddr,
    pub user: Option<User>,
    pub characters: Vec<Character>,
    pub state: LoginState,
    pub login_fail_count: u8,
    pub vac: bool,
    pub world_id: Option<u8>,
    pub channel_id: Option<u8>,
}
