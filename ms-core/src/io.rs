use std::io;
use std::io::prelude::*;

pub fn read_exact (mut r: &mut Read, length: usize) -> io::Result<Vec<u8>> {
    let mut buf = vec![0 as u8; length];
    let mut read = 0;

    while read < length {
        let inc = try!(r.read(&mut buf[read..]));
        read += inc;
    }
    Ok(buf)
}
