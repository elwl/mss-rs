use prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Version(pub u16);

impl binary::Writeable for Version {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        let &Version(v) = self;
        binary::write(w, &v)
    }
}
impl binary::Readable for Version {
    fn read (mut r: &mut Read) -> binary::Result<Version> {
        Ok(Version(try!(binary::read(r))))
    }
}
