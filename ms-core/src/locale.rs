use prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Locale(pub u8);

impl binary::Writeable for Locale {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        let &Locale(v) = self;
        binary::write(w, &v)
    }
}
impl binary::Readable for Locale {
    fn read (mut r: &mut Read) -> binary::Result<Locale> {
        Ok(Locale(try!(binary::read(r))))
    }
}
