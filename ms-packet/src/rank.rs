use prelude::*;

#[derive(Clone, Debug)]
pub struct Rank {
    pub world_rank: u32,
    pub world_rank_gap: u32,
    pub job_rank: u32,
    pub job_rank_gap: u32,
}

impl binary::Readable for Rank {
    fn read(r: &mut Read) -> binary::Result<Rank> {
        Ok(Rank {
            world_rank: try!(binary::read(r)),
            world_rank_gap: try!(binary::read(r)),
            job_rank: try!(binary::read(r)),
            job_rank_gap: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for Rank {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.world_rank.write(w));
        try!(self.world_rank_gap.write(w));
        try!(self.job_rank.write(w));
        self.job_rank_gap.write(w)
    }
}
