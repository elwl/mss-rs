use prelude::*;

#[derive(Clone, Debug)]
pub struct UpdatePin {
    pub error: bool,
}
impl binary::Readable for UpdatePin {
    fn read (r: &mut Read) -> binary::Result<UpdatePin> {
        Ok(UpdatePin {
            error: try!(binary::read(r))
        })
    }
}
impl binary::Writeable for UpdatePin {
    fn write (&self, mut w: &mut Write) -> binary::Result<()> {
        self.error.write(w)
    }
}
