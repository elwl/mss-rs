use prelude::*;

use ::avatar_look::AvatarLook;
use ::character_stat::CharacterStat;
use ::rank::Rank;

#[derive(Clone, Debug)]
pub struct SelectWorld {
    pub result: u8,
    pub characters: Vec<(CharacterStat, AvatarLook, Option<Rank>)>,
    pub slot_count: u32,
}

impl binary::Readable for SelectWorld {
    fn read(r: &mut Read) -> binary::Result<SelectWorld> {
        Ok(SelectWorld {
            result: try!(binary::read(r)),
            characters: {
                let mut chars = vec![];
                for _ in 0..try!(binary::read::<u32>(r)) {
                    chars.push((
                        try!(binary::read(r)),
                        try!(binary::read(r)),
                        if try!(binary::read(r)) {
                            Some(try!(binary::read::<Rank>(r)))
                        } else {
                            None
                        }
                    ));
                }
                chars
            },
            slot_count: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for SelectWorld {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.result.write(w));
        try!((self.characters.len() as u8).write(w));
        for (stats, avatar, ranking) in self.characters.clone() {
            try!(stats.write(w));
            try!(avatar.write(w));
            try!(ranking.is_some().write(w));
            if let Some(rank) = ranking {
                try!(rank.write(w));
            }
        }
        self.slot_count.write(w)
    }
}
