use prelude::*;

#[derive(Clone, Debug)]
pub struct CheckUserLimit {
    pub warning_level: u8,
    pub population_level: u8,
}

impl binary::Readable for CheckUserLimit {
    fn read(r: &mut Read) -> binary::Result<CheckUserLimit> {
        Ok(CheckUserLimit {
            warning_level: try!(binary::read(r)),
            population_level: try!(binary::read(r)),
        })
    }
}
impl binary::Writeable for CheckUserLimit {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.warning_level.write(w));
        self.population_level.write(w)
    }
}
