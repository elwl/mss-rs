use prelude::*;

#[derive(Clone, Copy, Debug)]
pub enum PinResult {
    Success = 0, 
    NotAssigned = 1, 
    Incorrect = 2, 
    DBFail = 3, 
    Assigned = 4, 
    AlreadyConnected = 7,
}
impl PinResult {
    pub fn from_u8(pin_result: u8) -> Option<PinResult> {
        match pin_result {
            0 => Some(PinResult::Success),
            1 => Some(PinResult::NotAssigned),
            2 => Some(PinResult::Incorrect),
            3 => Some(PinResult::DBFail),
            4 => Some(PinResult::Assigned),
            7 => Some(PinResult::AlreadyConnected),
            _ => None
        }
    }
}
impl binary::Readable for PinResult {
    fn read(r: &mut Read) -> binary::Result<PinResult> {
        Ok(PinResult::from_u8(try!(binary::read(r))).unwrap())
    }
}
impl binary::Writeable for PinResult {
    fn write(&self, mut w: &mut Write) -> binary::Result<()> {
        let pin_result: u8 = match *self {
            PinResult::Success => 0,
            PinResult::NotAssigned => 1,
            PinResult::Incorrect => 2,
            PinResult::DBFail => 3,
            PinResult::Assigned => 4,
            PinResult::AlreadyConnected => 7,
        };
        binary::write(w, &pin_result)
    }
}

#[derive(Clone, Debug)]
pub struct CheckPin {
    pub result: PinResult
}
impl binary::Readable for CheckPin {
    fn read(r: &mut Read) -> binary::Result<CheckPin> {
        Ok(CheckPin {
            result: try!(Readable::read(r))
        })
    }
}
impl binary::Writeable for CheckPin {
    fn write(&self, mut w: &mut Write) -> binary::Result<()> {
        self.result.write(&mut w)
    }
}
