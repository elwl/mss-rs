use prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct SetAccount {
    pub gender: mscore::Gender,
    pub successful: bool,
}
impl binary::Readable for SetAccount {
    fn read(r: &mut Read) -> binary::Result<SetAccount> {
        Ok(SetAccount {
            gender: try!(Readable::read(r)),
            successful: try!(Readable::read(r)),
        })
    }
}
impl binary::Writeable for SetAccount {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.gender.write(w));
        self.successful.write(w)
    }
}
