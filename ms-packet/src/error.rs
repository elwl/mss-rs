use mscore;

use std::convert;

#[derive(Debug)]
pub enum Error {
    UnknownPacketType(u16),
    Io(mscore::binary::Error),
}

impl convert::From<mscore::binary::Error> for Error {
    fn from (x: mscore::binary::Error) -> Error {
        Error::Io(x)
    }
}
