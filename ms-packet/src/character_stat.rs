use prelude::*;

use encoding::{Encoding, DecoderTrap, EncoderTrap};
use encoding::all::WINDOWS_949;

#[derive(Clone, Debug)]
pub struct CharacterStat {
    pub character_id: u32,
    pub character_name: String,
    pub gender: u8,
    pub skin: u8,
    pub face: u32,
    pub hair: u32,
    pub pet_serials: [u64; 3],
    pub level: u8,
    pub job: u16,
    pub str: u16,
    pub dex: u16,
    pub int: u16,
    pub luk: u16,
    pub hp: u16,
    pub max_hp: u16,
    pub mp: u16,
    pub max_mp: u16,
    pub ap: u16,
    pub sp: u16,
    pub exp: u32,
    pub fame: u16,
    pub map: u32,
    pub portal: u8
}

impl binary::Readable for CharacterStat {
    fn read(r: &mut Read) -> binary::Result<CharacterStat> {
        Ok(CharacterStat {
            character_id: try!(binary::read(r)),
            character_name: {
                let mut dst = [0u8; 13];
                for i in 0..dst.len() {
                    dst[i] = try!(binary::read(r));
                }
                WINDOWS_949.decode(&dst, DecoderTrap::Replace).unwrap()
            },
            gender: try!(binary::read(r)),
            skin: try!(binary::read(r)),
            face: try!(binary::read(r)),
            hair: try!(binary::read(r)),
            pet_serials: [try!(binary::read(r)), try!(binary::read(r)), try!(binary::read(r))],
            level: try!(binary::read(r)), 
            job: try!(binary::read(r)), 
            str: try!(binary::read(r)), 
            dex: try!(binary::read(r)), 
            int: try!(binary::read(r)), 
            luk: try!(binary::read(r)), 
            hp: try!(binary::read(r)), 
            max_hp: try!(binary::read(r)), 
            mp: try!(binary::read(r)), 
            max_mp: try!(binary::read(r)), 
            ap: try!(binary::read(r)),
            sp: try!(binary::read(r)),
            exp: try!(binary::read(r)),
            fame: try!(binary::read(r)),
            map: try!(binary::read(r)),
            portal: try!(binary::read(r)),
        })
    }
}

impl binary::Writeable for CharacterStat {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.character_id.write(w));
        let encoded = &*WINDOWS_949.encode(&*self.character_name, EncoderTrap::Replace).unwrap();
        try!(w.write_all(encoded));
        for _ in encoded.len()..13 {
            try!(0u8.write(w));
        }
        try!(self.gender.write(w));
        try!(self.skin.write(w));
        try!(self.face.write(w));
        try!(self.hair.write(w));
        try!(self.pet_serials[0].write(w));
        try!(self.pet_serials[1].write(w));
        try!(self.pet_serials[2].write(w));
        try!(self.level.write(w));
        try!(self.job.write(w));
        try!(self.str.write(w));
        try!(self.dex.write(w));
        try!(self.int.write(w));
        try!(self.luk.write(w));
        try!(self.hp.write(w));
        try!(self.max_hp.write(w));
        try!(self.mp.write(w));
        try!(self.max_mp.write(w));
        try!(self.ap.write(w));
        try!(self.sp.write(w));
        try!(self.exp.write(w));
        try!(self.fame.write(w));
        try!(self.map.write(w));
        self.portal.write(w)
    }
}
