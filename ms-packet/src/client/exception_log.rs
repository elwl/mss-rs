use prelude::*;

#[derive(Clone, Debug)]
pub struct ExceptionLog {
    exception: String,
}
impl binary::Readable for ExceptionLog {
    fn read(r: &mut Read) -> binary::Result<ExceptionLog> {
        Ok(ExceptionLog {
            exception: try!(Readable::read(r))
        })
    }
}
impl binary::Writeable for ExceptionLog {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        self.exception.write(w)
    }
}
