use prelude::*;

#[derive(Clone, Debug)]
pub struct SetGender {
    pub gender: mscore::Gender
}
impl binary::Readable for SetGender {
    fn read(r: &mut Read) -> binary::Result<SetGender> {
        let ok: bool = try!(Readable::read(r));
        Ok(if ok {
            SetGender {
                gender: try!(Readable::read(r))
            }
        } else {
            SetGender {
                gender: mscore::Gender::Undefined
            }
        })
    }
}
impl binary::Writeable for SetGender {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        if self.gender == mscore::Gender::Undefined {
            0u8.write(w)
        } else {
            try!(1u8.write(w));
            self.gender.write(w)
        }
    }
}
