use prelude::*;

#[derive(Clone, Debug)]
pub struct CheckUserLimit {
    pub world_id: u16,
}

impl binary::Readable for CheckUserLimit {
    fn read(r: &mut Read) -> binary::Result<CheckUserLimit> {
        Ok(CheckUserLimit {
            world_id: try!(binary::read(r)),
        })
    }
}
impl binary::Writeable for CheckUserLimit {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        self.world_id.write(w)
    }
}
