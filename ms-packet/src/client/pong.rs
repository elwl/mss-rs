use prelude::*;

#[derive(Clone, Debug)]
pub struct Pong;

impl binary::Readable for Pong {
    fn read(_: &mut Read) -> binary::Result<Pong> {
        Ok(Pong)
    }
}
impl binary::Writeable for Pong {
    fn write(&self, _: &mut Write) -> binary::Result<()> {
        Ok(())
    }
}
