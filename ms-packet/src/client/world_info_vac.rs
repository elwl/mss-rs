use prelude::*;

#[derive(Clone, Debug)]
pub struct WorldInfoVAC;

impl binary::Readable for WorldInfoVAC {
    fn read(_: &mut Read) -> binary::Result<WorldInfoVAC> {
        Ok(WorldInfoVAC)
    }
}

impl binary::Writeable for WorldInfoVAC {
    fn write(&self, _: &mut Write) -> binary::Result<()> {
        Ok(())
    }
}
