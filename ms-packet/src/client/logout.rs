use prelude::*;

#[derive(Clone, Debug)]
pub struct Logout;

impl binary::Readable for Logout {
    fn read(r: &mut Read) -> binary::Result<Logout> {
        //try!(binary::read::<u8>(r));
        Ok(Logout)
    }
}

impl binary::Writeable for Logout {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        Ok(())
    }
}
