use prelude::*;

#[derive(Clone, Copy, Debug)]
pub struct ConfirmEula {
    pub confirmed: bool,
}
impl binary::Readable for ConfirmEula {
    fn read(r: &mut Read) -> binary::Result<ConfirmEula> {
        Ok(ConfirmEula {
            confirmed: try!(Readable::read(r)),
        })
    }
}
impl binary::Writeable for ConfirmEula {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        self.confirmed.write(w)
    }
}
