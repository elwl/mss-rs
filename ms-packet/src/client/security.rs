use prelude::*;

#[derive(Clone, Copy, Debug)]
pub enum SecurityPacketType {
    MemoryRegion = 0x0,
    MemoryHash = 0x1,
    CheckClientIntegrity = 0x2,
}
impl SecurityPacketType {
    pub fn from_u8(packet_type: u8) -> Option<SecurityPacketType> {
        match packet_type {
            0x0 => Some(SecurityPacketType::MemoryRegion),
            0x1 => Some(SecurityPacketType::MemoryHash),
            0x2 => Some(SecurityPacketType::CheckClientIntegrity),
            _ => None
        }
    }
}
impl binary::Readable for SecurityPacketType {
    fn read(r: &mut Read) -> binary::Result<SecurityPacketType> {
        Ok(SecurityPacketType::from_u8(try!(binary::read(r))).unwrap())
    }
}
impl binary::Writeable for SecurityPacketType {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        let packet_type: u8 = match *self {
            SecurityPacketType::MemoryRegion => 0x0,
            SecurityPacketType::MemoryHash => 0x1,
            SecurityPacketType::CheckClientIntegrity => 0x2,
        };
        binary::write(w, &packet_type)
    }
}

#[derive(Clone, Debug)]
pub struct Security {
    pub packet_type: SecurityPacketType,
    pub crc_raw_seq: u32,
}
impl binary::Readable for Security {
    fn read(r: &mut Read) -> binary::Result<Security> {
        let packet_type: SecurityPacketType = try!(Readable::read(r));
        Ok(match packet_type {
            SecurityPacketType::MemoryHash => {
                Security {
                    packet_type: packet_type,
                    crc_raw_seq: try!(Readable::read(r))
                }
            },
            _ => unimplemented!()
        })
    }
}
impl binary::Writeable for Security {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.packet_type.write(w));
        match self.packet_type {
            SecurityPacketType::MemoryHash =>
                try!(self.crc_raw_seq.write(w)),
            _ => unimplemented!()
        }
        Ok(())
    }
}
