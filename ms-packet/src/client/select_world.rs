use prelude::*;

#[derive(Clone, Debug)]
pub struct SelectWorld {
    pub world_id: u8,
    pub channel_id: u8,
}

impl binary::Readable for SelectWorld {
    fn read(r: &mut Read) -> binary::Result<SelectWorld> {
        Ok(SelectWorld {
            world_id: try!(binary::read(r)),
            channel_id: try!(binary::read(r)),
        })
    }
}
impl binary::Writeable for SelectWorld {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.world_id.write(w));
        self.channel_id.write(w)
    }
}
