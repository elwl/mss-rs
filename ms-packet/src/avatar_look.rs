use prelude::*;

#[derive(Clone, Debug)]
pub struct AvatarLook {
    pub gender: u8,
    pub skin: u8,
    pub face: u32,
    pub megaphone: bool,
    pub hair: u32,
    pub equips: Vec<(u8, u32)>,
    pub cash_equips: Vec<(u8, u32)>,
    pub pets: [u32; 3],
}

impl binary::Readable for AvatarLook {
    fn read(r: &mut Read) -> binary::Result<AvatarLook> {
        Ok(AvatarLook {
            gender: try!(binary::read(r)),
            skin: try!(binary::read(r)),
            face: try!(binary::read(r)),
            megaphone: try!(binary::read(r)),
            hair: try!(binary::read(r)),
            equips: {
                let mut ret = vec![];
                while let Ok(slot) = binary::read::<u8>(r) {
                    if slot == 0xFF {
                        break;
                    }
                    ret.push((slot, try!(binary::read::<u32>(r))));
                }
                ret
            },
            cash_equips: {
                let mut ret = vec![];
                while let Ok(slot) = binary::read::<u8>(r) {
                    if slot == 0xFF {
                        break;
                    }
                    ret.push((slot, try!(binary::read::<u32>(r))));
                }
                ret
            },
            pets: [try!(binary::read(r)), try!(binary::read(r)), try!(binary::read(r))],
        })
    }
}

impl binary::Writeable for AvatarLook {
    fn write(&self, w: &mut Write) -> binary::Result<()> {
        try!(self.gender.write(w));
        try!(self.skin.write(w));
        try!(self.face.write(w));
        try!(self.megaphone.write(w));
        try!(self.hair.write(w));

        for &(slot, equip) in &self.equips {
            try!(slot.write(w));
            try!(equip.write(w));
        }
        try!(0xFFu8.write(w));

        for &(slot, equip) in &self.cash_equips {
            try!(slot.write(w));
            try!(equip.write(w));
        }
        try!(0xFFu8.write(w));
        try!(0u32.write(w)); // weapon sticker, aka cash weapon cover -_-
        self.pets.iter().map(|pet_id| pet_id.write(w)).count();
        Ok(())
    }
}
